package ep2;

public class Missil extends Sprite{
	
	private static final int MISSIL_SPEED = -2;
	
	public Missil(int x, int y){
		super(x, y);
		loadImage("images/missile.png");
	}

	public void move(){
		y+= MISSIL_SPEED;
		if(y<0){
			this.setVisible(false);
		}
	}
}
