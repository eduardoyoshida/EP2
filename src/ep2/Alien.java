package ep2;


public class Alien extends Sprite {
	
	private int nivel;
	private int speed;
	private boolean destruida;
	
	public Alien(int x, int nivel){
		super(x, 0);
		this.nivel = nivel;
		destruida = false;
		initAlien();
	}
	
	public int getNivel(){
		return nivel;
	}
	
	private void initAlien(){
		if(nivel == 1){
			loadImage("images/alien_EASY.png");
			speed = 1;
		}
		if(nivel == 2){
			loadImage("images/alien_MEDIUM.png");
			speed = 2;
		}
		if(nivel == 3){
			loadImage("images/alien_HARD.png");
			speed = 1;
		}
	}
	
	public void explodirAlien(){
		loadImage("images/explosion.png");
		speed = -1;
		destruida = true;
	}
		
	public boolean isDestruida(){
		return destruida;
	}
	
	public void move(){//Polimorfismo
		if(nivel == 3){
			if(isDestruida()){
				y += speed;
			}else{
				if(x<=0){
					speed = 1;
				}
				if(x>=Game.getWidth()){
					speed = -1;
				}
				x += speed;
			}
		}else{
			y += speed;
		}
	}
	
    public MissilAlien atirar(){
    	return new MissilAlien(x, y-2);
    }
}
