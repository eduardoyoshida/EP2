package ep2;

import java.awt.Color;
import java.util.Random;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import java.util.ArrayList;

public class Map extends JPanel implements ActionListener {

	private static final long serialVersionUID = 6314048367041846168L;
	private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private Timer timer_map;
    private final int PONTUACAO_MEDIO = 20;
    private final int PONTUACAO_DIFICIL = 50;
    private static final int DELAY = 60;
    private static final int CHANCE_ATIRAR = 6;
    
    private int contadorHard = 0;
    private final Image background;
    private final Spaceship spaceship;
    private ArrayList<Alien> aliens;
    private ArrayList<MissilAlien> missilAlien;
    private int dificuldade = 5;
    private int nivel = 2;
    private boolean inGame = true;
    private int delay = 0;
    
    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();
        
        aliens = new ArrayList<>();
        
        missilAlien = new ArrayList<>();
        
        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
        
    }
    
	private synchronized void gerarAliens() {
		Random gerador = new Random();
		for(int i=0;i<dificuldade;i++){
			int posicao = gerador.nextInt(Game.getWidth());
			int alienNivel = gerador.nextInt(nivel);
			if(nivel == 3 && contadorHard >=6){
				continue;
			}else{
				if(nivel == 3){
					contadorHard++;
				}
				aliens.add(new Alien(posicao, alienNivel));
			}
		}		
	}
	
	private synchronized void atualizarNivel(){
		if(spaceship.getPontuacao() > PONTUACAO_MEDIO){
			nivel = 3;//seria nível 2, mas por conta do gerador de números aleatórios precisa ser 2 + 1;
		}
		if(spaceship.getPontuacao() > PONTUACAO_DIFICIL){
			nivel = 4;
		}
	}
        
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.background, 0, 0, null);
        if(inGame){
        draw(g);
        }else{
        drawGameOver(g);
        }
        Toolkit.getDefaultToolkit().sync();
    }
    
    private void draw(Graphics g) {
        //Desenha os objetos na tela (spaceship e misseis)
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        
        ArrayList<Missil> misseis = spaceship.getMisseis();

        for (Missil missil : misseis) {
            if (missil.isVisible()) {
                g.drawImage(missil.getImage(), missil.getX(), missil.getY(), this);
            }
        }
        for (Alien alien : aliens){
        	if(alien.isVisible()){
        		g.drawImage(alien.getImage(), alien.getX(), alien.getY(), this);
        	}
        }
        
        for(MissilAlien missil : missilAlien){
        	if(missil.isVisible()){
        		g.drawImage(missil.getImage(), missil.getX(), missil.getY(), this);
        	}
        }
        g.setColor(Color.yellow);
        g.drawString("Pontuação: " + spaceship.getPontuacao(), 5, 15);
        g.drawString("Pressione 'p' para pausar", Game.getWidth() - 175, 15);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
        atualizarMisseis();
        atualizarAliens();
        atualizarNivel();  
        atualizarMissilAlien();
        checarColisaoNave();
        checarColisaoMissil();
        if(inGame){
            updateSpaceship();
        	geradorTirosAliens();
        	if(delay == DELAY){
        	gerarAliens();
        	delay = 0;
        	}else{
        		delay++;
        	}
        }
        repaint();
    }
    
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        String pontuacao = "Aliens Destruídos: " + spaceship.getPontuacao();
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        Font font2 = new Font("Helvetica", Font.BOLD, 10);
        g.setColor(Color.white);
        g.setFont(font2);
        g.drawString(pontuacao, (Game.getWidth() - metric.stringWidth(pontuacao)+47) / 2, Game.getHeight() / 2 + 15);
    }
    
    private synchronized void updateSpaceship() {
        spaceship.move();
    }
  

    private class KeyListerner extends KeyAdapter {    	
        @Override
        public void keyPressed(KeyEvent e) {
        	if(e.getKeyCode() == KeyEvent.VK_P ){
        		if(timer_map.isRunning()){
        			timer_map.stop();
        		}else{
        			timer_map.start();
        		}
        	}
            spaceship.keyPressed(e);
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }   
    }
    
    private synchronized void atualizarMisseis(){
    	ArrayList<Missil> misseis = spaceship.getMisseis();
    	
    	for(int i = 0; i < misseis.size(); i++){
    		Missil missil = misseis.get(i);
    		if(missil.isVisible()){
    			missil.move();
    		}else{
    			misseis.remove(i);
    		}
    	}
    	
    }
    
    private synchronized void atualizarAliens(){
    	for(int i = 0; i < aliens.size(); i++){
    		Alien alien = aliens.get(i);
    		if(alien.isVisible()){
    			alien.move();
    		}else{
    			if(alien.getNivel()== 3){
    				contadorHard--;
    			}
    			aliens.remove(i);
    		}
    	}
    }
    
    
    private synchronized void checarColisaoNave(){
    		Rectangle naveHitBox = spaceship.getBounds();
    		for(Alien alien : aliens){
    		Rectangle alienHitBox = alien.getBounds();
    		if(naveHitBox.intersects(alienHitBox)){
    			spaceship.explodirNave();
    			inGame = false;	
    		}
    		}
    		for(MissilAlien missil : missilAlien){
    			Rectangle missilAlienHitBox = missil.getBounds();
    			if(naveHitBox.intersects(missilAlienHitBox)){
    				spaceship.setVisible(false);
    				inGame = false;
    			}
    		}
    }
    
    private synchronized void checarColisaoMissil(){
    		ArrayList<Missil> misseis = spaceship.getMisseis();
    		for(Missil missil : misseis){
    		Rectangle missilHitBox = missil.getBounds();
    			for(Alien alien : aliens){
    				Rectangle alienHitBox = alien.getBounds();
    				if(missilHitBox.intersects(alienHitBox)){
    					alien.explodirAlien();
    					missil.setVisible(false);
    					spaceship.addPonto();
    				}
    			}
    		}
    }
    
    private synchronized void geradorTirosAliens(){
    	Random geradorTiros = new Random();
    	int chance;
    	for(Alien alien : aliens){
    		if(alien.getNivel() == 3){
    			chance = geradorTiros.nextInt(1000);
    			if(chance == CHANCE_ATIRAR){
    				missilAlien.add(alien.atirar());
    			}
    		}
    	}
    }
    
    private synchronized void atualizarMissilAlien(){
    	for(MissilAlien missil : missilAlien ){
    		missil.move();
    	}
    }    
}