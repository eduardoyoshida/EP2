package ep2;

public class MissilAlien extends Sprite {
	private static final int MISSIL_SPEED = 1;
	
	public MissilAlien(int x, int y){
		super(x, y);
		loadImage("images/alien_missile.png");
	}
	
	public void move(){
		y += MISSIL_SPEED;
	}
}
